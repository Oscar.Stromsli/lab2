package INF101.lab2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    ArrayList<FridgeItem> fridgeItems = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return fridgeItems.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (fridgeItems.size()<20) {
            fridgeItems.add(item);
            return true;
        } else
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeItems.contains(item)) {
            fridgeItems.remove(item);
        } else throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {
        fridgeItems.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        List<FridgeItem> expiredItems = new LinkedList<>();

        for (FridgeItem item : fridgeItems) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }

        for (FridgeItem item : expiredItems) {
            takeOut(item);
        }
        return expiredItems;
    }
}
